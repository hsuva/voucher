# Installation
#### Install Packages
```
composer install
```

#### Copy .env file
```
cp .env.example .env
```

#### Use mailtrap.io and copy smtp credentials to `.env`

#### Set database detail
```
php artisan migrate
```

#### Generate default admin user
```
php artisan db:seed --class=UserSeeder
```
*email : user@voucher.test* <br />
*password : Pass.word123*

#### Install NPM Dependencies
```
npm install
npm run dev
```

#### Start Voucher App
```
php artisan serve
```