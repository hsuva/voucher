<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use App\Models\Voucher;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AuthControllerTest extends TestCase
{
	use RefreshDatabase;

	public function testLoginSuccess()
	{
		$user = User::factory()->create();

		$response = $this->post("/api/login", [
				'email' => $user->email,
				'password' => 'password'
		])
		->assertOk();

		$this->assertAuthenticatedAs($user, $guard = null);
	}

	public function testLoginFailed()
	{
		$user = User::factory()->create();

		$response = $this->post("/api/login", [
				'email' => $user->email,
				'password' => ''
		])
		->assertUnauthorized();

		$this->assertGuest($guard = null);
	}

	public function testRegisterSuccess()
	{
		$user = User::factory()->make();

		$response = $this->post("/api/register", [
				'email' => $user->email,
				'name' => $user->name,
				'password' => 'password',
				'confirm_password' => 'password'
		])
		->assertOk();
	}

	public function testRegisterFailed()
	{
		$user = User::factory()->make();

		$response = $this->post("/api/register", [
				'email' => $user->email,
				'name' => $user->name,
				'password' => 'password',
				'confirm_password' => 'pass'
		])
		->assertStatus(302);
	}

	public function testRegisterUniqueEmail()
	{
		$user = User::factory()->create();

		$response = $this->post("/api/register", [
				'email' => $user->email,
				'name' => $user->name,
				'password' => 'password',
				'confirm_password' => 'password'
		])
		->assertStatus(302);
	}
}
