<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use App\Models\Voucher;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\RefreshDatabase;

class VoucherControllerTest extends TestCase
{
	use RefreshDatabase;

	public function testGetLoggedInUser()
	{
		$user = Sanctum::actingAs(User::factory()->create());

		$response = $this->post("/api/me")
			->assertExactJson($user->toArray());
	}

	public function testGetUserVouchers()
	{
		$user1 = Sanctum::actingAs(User::factory()->create());
		$user2 = User::factory()->create();
	
		Voucher::factory()->count(3)->create(['user_id' => $user1->id]);	
	
		$response = $this->get('/api/vouchers')
			->assertStatus(200)
			->assertJsonCount(4, 'data')
			->assertJsonMissing(['user_id' => $user2->id])
			->assertJsonFragment(['data' => Voucher::where('user_id', $user1->id)->get()->toArray()]);
	}

	public function testGenerateVoucher()
	{
		$user = Sanctum::actingAs(User::factory()->create());
		
		$response = $this->post('/api/vouchers')
			->assertCreated()
			->assertJsonFragment(['user_id' => $user->id]);
	}

	public function testDeleteVoucher()
	{
		$user = Sanctum::actingAs(User::factory()->create());

		$voucher = Voucher::factory()->create();
		
		$response = $this->delete("/api/vouchers/$voucher->id")
			->assertNoContent();
		
		$this->assertNull(Voucher::find($voucher->id));
	}
}
