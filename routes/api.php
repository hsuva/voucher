<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\VoucherController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\ReportController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', [AuthController::class, 'register']);
Route::post('login', [AuthController::class, 'login']);

Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::post('me', [AuthController::class, 'me']);
    Route::post('logout', [AuthController::class, 'logout']);
		Route::apiResource('vouchers', VoucherController::class)->except(['update', 'show']);

		Route::get('users', [UserController::class, 'index'])->middleware('admin');
		Route::get('reports/voucher/csv', [ReportController::class, 'voucher_csv'])->middleware('admin');
		Route::get('reports/voucher/metrics', [ReportController::class, 'voucher_metrics'])->middleware('admin');
});