<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVouchersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
			if (!Schema::hasTable('vouchers')) {
				Schema::create('vouchers', function (Blueprint $table) {
            $table->id();
						$table->string('code');
						$table->unsignedBigInteger('user_id');
            $table->timestamps();

						$table->foreign('user_id')->references('id')->on('users');
        });
			}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vouchers');
    }
}
