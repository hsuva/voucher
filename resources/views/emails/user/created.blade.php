@component('mail::message')
# Voucher Code

<h1>{{ $voucher->code }}</h1>

{{-- @component('mail::button', ['url' => ''])
Button Text
@endcomponent --}}

Thanks,<br>
{{ config('app.name') }} Team
@endcomponent
