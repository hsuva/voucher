import axios from 'axios'
import router from '../router'

export default {
    namespaced: true,
    state:{
        authenticated:false,
        user:{},
				token: ''
    },
    getters:{
        authenticated(state){
            return state.authenticated
        },
        user(state){
            return state.user
        },
				token(state){
            return state.token
        }
    },
    mutations:{
        SET_AUTHENTICATED (state, value) {
            state.authenticated = value
        },
        SET_USER (state, value) {
            state.user = value
        },
				SET_TOKEN (state, value) {
            state.token = value
        }
    },
    actions:{
        login({commit}, data){
					let token = data.access_token;
					return axios.post('/api/me', {}, { 'headers': { 'Authorization':  'Bearer ' + token }}).then(({data})=>{
							commit('SET_USER',data)
							commit('SET_AUTHENTICATED',true)
							commit('SET_TOKEN',token)
							if (data.role == 'admin') {
								router.push({name: 'admin.dashboard'})
							} else {
								router.push({name:'dashboard'})
							}
					}).catch(({response:{data}})=>{
							commit('SET_USER',{})
							commit('SET_AUTHENTICATED',false)
					})
        },
        logout({commit}){
            commit('SET_USER',{})
            commit('SET_AUTHENTICATED',false)
        }
    }
}