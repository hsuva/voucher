/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue').default;

import axios from 'axios';
import router from './router';
import store from './store';

axios.defaults.withCredentials = true
axios.defaults.baseURL = 'http://localhost:8000/'

const app = new Vue({
    el: '#app',
    router:router,
    store:store
});