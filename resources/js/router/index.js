import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store'

Vue.use(VueRouter)

/* Guest Component */
const Login = () => import('../components/Login.vue')
const Register = () => import('../components/Register.vue')

/* Layouts */
const App = () => import('../components/Layouts/App.vue')

/* Authenticated Component */
const Dashboard = () => import('../components/Dashboard.vue')
const AdminDashboard = () => import('../components/AdminDashboard.vue')
const AdminUsers = () => import('../components/Users.vue')

const Routes = [
    {
        name:"login",
        path:"/login",
        component:Login,
        meta:{
            middleware:"guest",
            title:`Login`
        }
    },
    {
        name:"register",
        path:"/register",
        component:Register,
        meta:{
            middleware:"guest",
            title:`Register`
        }
    },
    {
        path:"/",
        component:App,
        meta:{
            middleware:"auth"
        },
        children:[
            {
                name:"dashboard",
                path: '/',
                component: Dashboard,
                meta:{
                    title:`Dashboard`,
                }
            }
        ]
    },
		{
			path:"/admin",
			component: App,
			meta: {
				middleware:"admin"
			},
			children: [
				{
					name: "admin.dashboard",
					path: '/admin',
					component: AdminDashboard,
					meta:{
						title:`Admin`,
						middleware:"admin"
					}
				},
				{
					name: "admin.users",
					path: '/admin/users',
					component: AdminUsers,
					meta:{
						title:`Users`,
						middleware:"admin"
					}
				}
			]
		}
]

var router  = new VueRouter({
    mode: 'history',
    routes: Routes
})

router.beforeEach((to, from, next) => {
    document.title = `${to.meta.title} - ${process.env.MIX_APP_NAME}`
    if(to.meta.middleware=="guest"){
        if(store.state.auth.authenticated){
            next({name:"dashboard"})
        }
        next()
		} 
		else{
        if(store.state.auth.authenticated){
					if(to.meta.middleware == 'admin' && store.state.auth.user.role == 'user') {
						next({name:"dashboard"})
					} else {
						next()
					}
        }else{
            next({name:"login"})
        }
    }
})

export default router