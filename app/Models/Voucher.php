<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Voucher extends Model
{
    use HasFactory;

		protected $fillable = [
			'user_id',
			'code'
		];

		public function user()
		{
			return $this->belongsTo(User::class);
		}

		/**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::creating(function ($voucher) {
					$voucher->code = strtoupper(uniqid());
        });
    }

		public function getCreatedAtAttribute($value)
		{
				return Carbon::parse($value)->format('M d, Y H:i:s');
		}
}
