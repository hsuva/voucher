<?php

namespace App\Http\Controllers;

use App\Models\Voucher;
use Illuminate\Http\Request;
use App\Http\Resources\VoucherResource;

class VoucherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return VoucherResource
     */
    public function index()
    {
			$vouchers = Voucher::where('user_id', \Auth::id())
				->latest()
				->paginate(5);
      return VoucherResource::collection($vouchers);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return VoucherResource
     */
    public function store()
    {
			if(\Auth::user()->vouchers->count() > 10) {
				return response()->json([
					'message' => 'You can only generate 10 voucher code.'
				], 401);
			}

			$voucher = Voucher::create([
				'user_id' => \Auth::id()
			]);

      return new VoucherResource($voucher);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Voucher  $voucher
     * @return \Illuminate\Http\Response
     */
    public function destroy(Voucher $voucher)
    {
        $voucher->delete();
				return response()->noContent();
    }
}
