<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;

class UserController extends Controller
{
	public function index()
	{
		$users = User::latest()
			->withCount('vouchers')
			->get();
    return UserResource::collection($users);
	}
}
