<?php

namespace App\Http\Controllers\Admin;

use App\Models\Voucher;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReportController extends Controller
{
    public function voucher_csv()
		{
			$vouchers = Voucher::all();
			$vouchers->load('user');
      return response()->json($vouchers);
		}

		public function voucher_metrics()
		{
			$vouchers = Voucher::select(
					\DB::raw('DATE_FORMAT(created_at,"%b %d, %Y %H:%i") as date'),
					\DB::raw('count(code) as count')
				)
				->groupBy(\DB::raw('DATE_FORMAT(created_at,"%b %d, %Y %H:%i")'))
				->orderBy('date')
				->get();

				return response()->json($vouchers);
		}
}
